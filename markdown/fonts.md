# Fonts

## Variable Width

The best font for **readability** is **Arial**.

Arial has a slight edge in readability over Tahoma due to Tahoma's eccessively
slanted italics.

The best font for **clarity** when distinguishing letters is **Tahoma**, but
bold versions of the typeface are relatively wide at large point sizes.

When space is limited, the best font for both **clarity** and **compactness**
is **Segoe UI**.  Plus Segoe UI renders some Unicode symbol glyphs more
clearly than Tahoma.

Tahoma is both compact and clearly distinguishes commonly misidentified
letters such as lowercase `L`, uppercase `I`, and the numeral `1`.

| Font                 | Example                                                                                     | Notes                                                                                                | 
| -------------------- | ------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------- | 
| Arial                | <span style="font-family: Arial;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>                | Cannot distinguish lowercase `L` and uppercase `I` at all                                            |
| Calibri              | <span style="font-family: Calibri;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>              | Hard to distinguish lowercase `L` and uppercase `I`                                                  | 
| DejaVu Sans          | <span style="font-family: DejaVu Sans;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>          | Hard to distinguish lowercase `L` and uppercase `I`                                                  | 
| Microsoft Sans Serif | <span style="font-family: Microsoft Sans Serif;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span> | Cannot distinguish lowercase `L` and uppercase `I` at all                                            | 
| Roboto               | <span style="font-family: Roboto;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>               | Hard to distinguish lowercase `L` and numeral `1`, angle bracket font hints not great                | 
| Segoe UI             | <span style="font-family: Segoe UI;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>             | Good clarity for individual letters, but readability is hurt by the relatively short x-height        | 
| Tahoma               | <span style="font-family: Tahoma;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>               | Clear distinction between similar letters, `,` and `.`; bold is relatively wide at large point sizes |
| Verdana              | <span style="font-family: Verdana;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>              | Similar to Tahoma but very wide                                                                      | 

| Font                 | Bold                     | Italics                | Bold Italics                       |
| -------------------- | ------------------------ | ---------------------- | ---------------------------------- |
| Arial                | **Arial**                | *Arial*                | <b><i>Arial</i></b>                |
| Calibri              | **Calibri**              | *Calibri*              | <b><i>Calibri</i></b>              |
| DejaVu Sans          | **DejaVu Sans**          | *DejaVu Sans*          | <b><i>DejaVu Sans</i></b>          |
| Microsoft Sans Serif | **Microsoft Sans Serif** | *Microsoft Sans Serif* | <b><i>Microsoft Sans Serif</i></b> |
| Roboto               | **Roboto**               | *Roboto*               | <b><i>Roboto</i></b>               |
| Segoe UI             | **Segoe UI**             | *Segoe UI*             | <b><i>Segoe UI</i></b>             |
| Tahoma               | **Tahoma**               | *Tahoma*               | <b><i>Tahoma</i></b>               |
| Verdana              | **Verdana**              | *Verdana*              | <b><i>Verdana</i></b>              |

## Fixed Width

The best font for **readability** and **clarity** is **DejaVu Sans Mono**.

Consolas is a close second in terms of clarity, but it has some issues with
distinguishing some commonly confused letters.

| Font                     | Example                                                                                         | Notes                                                                       | 
| ------------------------ | ----------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------- | 
| Bitstream Vera Sans Mono | <span style="font-family: Bitstream Vera Sans Mono;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span> | Same typeface design as DejaVu Sans Mono but with fewer supported glyphs    | 
| Consolas                 | <span style="font-family: Consolas;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>                 | More compact than DejaVu, but easy to confuse lowercase `L` and numeral `1` | 
| Courier New              | <span style="font-family: Courier New;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>              | Easy to confuse uppercase `O` and zero `0`                                  | 
| DejaVu Sans Mono         | <span style="font-family: DejaVu Sans Mono;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>         | Clear distinction between similar letters, but less compact than Consolas   |
| Lucida Console           | <span style="font-family: Lucida Console;">Oo0LlIi1\`’,.\<\>[]{}-=_+\|@#$%^&\*</span>           | Easy to confuse uppercase `O` and zero `0`                                  | 

| Font                     | Bold                         | Italics                    | Bold Itatics                           |
| ------------------------ | ---------------------------- | -------------------------- | -------------------------------------- |
| Bitstream Vera Sans Mono | **Bitstream Vera Sans Mono** | *Bitstream Vera Sans Mono* | <b><i>Bitstream Vera Sans Mono</i></b> |
| Consolas                 | **Consolas**                 | *Consolas*                 | <b><i>Consolas</i></b>                 |
| Courier New              | **Courier New**              | *Courier New*              | <b><i>Courier New</i></b>              |
| DejaVu Sans Mono         | **DejaVu Sans Mono**         | *DejaVu Sans Mono*         | <b><i>DejaVu Sans Mono</i></b>         |
| Lucida Console           | **Lucida Console**           | *Lucida Console*           | <b><i>Lucida Console</i></b>           |

Title:  Markdown Examples  
Author: Roger Bolsius  
CSS:    markdown.css  
Parent: index.html  

# Markdown Examples

This is text before the first second-level heading.

## Emphasis and Strong

This is an example of _italics_.  This word is an example of __bold__.

This is an example of identifier_with_underscores.  This word is an example of identifier__with__double__underscores.

This is an example of _identifier_surrounded_with_underscores_.  This word is an example of __identifier__surrounded__with__double__underscores__.

This is an example of _identifier_beginning_with_underscore.  This word is an example of __identifier__beginning__with__double__underscores.

This is an example of identifier_ending_with_underscore_.  This word is an example of identifier__ending__with__double__underscores__.

This is an example of *italics using asterisks*.  This word is an example of **bold using asterisks**.

This is an example of *italics*containing*asterisks*.  This word is an example of **bold**containing**asterisks**.

### Complex Examples

__bold__

abcd __bold__

__bold__ abcd

**bold**

**bold***

***bold**

__bold___

___bold__

(_italics_)

[_italics_](<test.html>)

, _italics_,

, _italics_ ,

!_italics_@

_italics_

__italics_

**italics*

*italics**

**bold and *italics***

**bold and _italics_**

***italics* and bold**

**bold and __bold__**

**bold and \*escaped\* asterisks**

**multiline
bold**

### Not Italics

.\_notitalics_

,\_notitalics_,

,\_notitalics_ ,

abc_notitalics_def

1_notitalics_2

$\_notitalics_#

_notitalics

notitalics_

### Unclear but Probably Should Be Italics

._italics_

,_italics_,

,_italics_ ,

$_italics_#

## Block Quoted Text

> Block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted block quoted block quoted block quoted block quoted block quoted
> block quoted
>
> > Block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted
> >
> > > Block quoted block quoted block quoted block quoted block quoted block quoted
> > > block quoted block quoted block quoted block quoted block quoted block quoted
> > > block quoted block quoted block quoted block quoted block quoted block quoted
> > > block quoted block quoted block quoted block quoted block quoted block quoted
> > >
> > >     Preformatted preformatted preformatted preformatted preformatted preformatted
> > >     Preformatted preformatted preformatted preformatted preformatted preformatted
> > >     Preformatted preformatted preformatted preformatted preformatted preformatted
> > >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >
> > Block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted
> > block quoted block quoted block quoted block quoted block quoted block quoted

## Preformatted Block Text

    Preformatted preformatted preformatted preformatted preformatted preformatted
    preformatted preformatted preformatted preformatted preformatted preformatted
    
    preformatted preformatted preformatted preformatted preformatted preformatted
    preformatted preformatted preformatted preformatted preformatted preformatted

Wide preformatted text:

    Preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted preformatted

## Inline Preformatted Text

This is inline `preformatted`.

## Links

This is an [inline_link](http://example.com/inline_link.html).

This is a local [inline_link](inline_link.html).

This is a [[Wiki Link]].

This is a [ref][] style link.

  [ref]: http://example.com/ref_style_link.html

A link without markdown http://example.com/inline_link.html.

A link without markdown: http://example.com/inline_link.html

http://example.com/inline_link.html

## Images

![An optional caption](book.jpg)

# First Level header

## Second Level Header

### Third Level Header

#### Fourth Level Header

##### Fifth Level Header

###### Sixth Level Header

Title-Style First Level Header
==============================

Section-Style Second Level Header
---------------------------------

## Horizontal Rules

Some text.

* * * * *

Some text.

----------------------------------------------------------------------

Some text.

## Footnotes

Here is some text containing a footnote.[^somesamplefootnote]  A footnote to a link.[^somelink]

Footnotes within a table:

| Item  | Notes |
| ----- | ----- |
| one   | [^1]  |
| two   | [^2]  |
| three | [^3]  |
| four  | [^4]  |
| five  | [^5]  |
| six   | [^6]  |
| seven | [^7]  |
| eight | [^8]  |
| nine  | [^9]  |
| ten   | [^10] |

[^somesamplefootnote]: Here is the text of the footnote itself.
[^somelink]: <http://somelink.com>
[^1]: footnote 1
[^2]: footnote 2
[^3]: footnote 3
[^4]: footnote 4
[^5]: footnote 5
[^6]: footnote 6
[^7]: footnote 7
[^8]: footnote 8
[^9]: footnote 9
[^10]: footnote 10

## Tables

|              |          Grouping           ||
| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content      |         *Long Cell*         ||
| Content      |   **Cell**    |         Cell |

| New section  |     More      |         Data |
| And more     |          And more           ||


## Data Type Limitations

| Data type     | Limitations                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| ------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BIG INT       | `BIG INT` is not supported by JDBC or the BI Admintool.  So, `BIG INT` is not really supported by BI EE.  However, internally the BI Server does have some support for `BIG INT`, but it is not well tested.  `BIG INT` is intended to be same as the C `int64` data type.                                                                                                                                                                                                                                                                                                                                                                                        |
| BINARY        | `BINARY` is not supported very well in BI EE.  The BI Server only supports fetching of `BINARY` columns.  The BI Server does not support `BINARY` in bind parameters or insert statements.                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| BIT           | `BIT` is not officially supported.  There are several bugs related to the `BIT` data type (for example, the internal bug 17828069).  Customers should use `INT` or `CHAR` instead of `BIT` to represent Boolean data.                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| CHAR          | `CHAR` values are always padded with spaces at the end up to the length specified by the data type.  `CHAR` supports Unicode values.  On windows the storage is two bytes per character.  On all Unix 64-bit platforms `CHAR` is represented using four bytes per character.                                                                                                                                                                                                                                                                                                                                                                                      |
| DATE          | `DATE` represents only year, month, and day components,  Unlike the Oracle `DATE` data type it does not represent hours, minutes, or seconds.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| DECIMAL       | `DECIMAL` is the same as `NUMERIC` in 11.1.1.7.0 and later.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| DOUBLE        | `DOUBLE` is the same as the IEEE 754 64-bit double precision binary floating point data type. The internal storage is eight bytes.  The significand occupies 53 bits (including the sign bit).  So, the precision is limited to about 16 decimal digits (2<sup>53</sup> ≅ 10<sup>15.95</sup>).  The exponent occupies 11 bits.  The range of the exponent is approximately ±307 as a base 10 decimal value.                                                                                                                                                                                                                                                       |
| INTEGER       | `INTEGER` is a signed binary integer data type occupying four bytes.  The maximum value that can be represented is 2,147,483,647, and the minimum value is -2,147,483,648.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| FLOAT         | `FLOAT` is the same as the IEEE 754 32-bit single precision binary floating point data type. The internal storage is four bytes.  The significand occupies 24 bits (including the sign bit).  So, the precision is limited to about 7 decimal digits (2<sup>24</sup> ≅ 10<sup>7.22</sup>).  The exponent occupies 8 bits.  The range of the exponent is approximately ±38 as a base 10 decimal value.                                                                                                                                                                                                                                                             |
| LONGVARBINARY | `LONGVARBINARY` supports up to 32,678 bytes.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| LONGVARCHAR   | `LONGVARCHAR` supports up to 32,678 bytes.  Both `LONGVARCHAR` and `VARCHAR` (see below) support Unicode values.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| NUMERIC       | `NUMERIC` is supported by BI EE 11.1.1.7.0 and later.  It is a true decimal data type occupying 22 bytes.  The internal representation and limitations are exactly the same as the Oracle `NUMBER` data type.  `NUMERIC` supports positive numbers in the range of 1 x 10<sup>-130</sup> to 9.999...9 x 10<sup>125</sup> with up to 38 significant digits.  As of the current version of BI EE the precision and scale are not stored in the BI repository.  The scale is assumed to be 10.                                                                                                                                                                       |
| REAL          | Same as `FLOAT`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| SMALLINT      | `SMALLINT` is represented as an `INTEGER` internally in the BI Server and is subject to the same limitations as the `INTEGER` data type.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| TIME          | `TIME` represents only hour, minute, and second components.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| TIMESTAMP     | `TIMESTAMP` represents year, month, day, hour, minute, and second components.  On some data sources and platforms it may also support fractions of a second.  The NQSConfig.INI parameter `DATE_TIME_DISPLAY_FORMAT` accepts the format "yyyy/mm/dd hh:mi:ss nssssssss".  However, this format string only affects the output for the nqserver.log when the log format is set to `classic` as opposed to the standard `ODL` log format.  In the BI repository you can specify the same nanosecond precision.  In logical SQL queries you can also express timestamp literals with nanosecond precision (for example `timestamp '2013-08-01 11:00:00.800000000'`). |
| TINYINT       | `TINYINT` is represented as an `INTEGER` internally in the BI Server and is subject to the same limitations as the `INTEGER` data type.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| VARBINARY     | `VARBINARY` is interchangable with `LONGVARBINARY` and has the same limitations as `LONGVARBINARY`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| VARCHAR       | `VARCHAR` is interchangable with `LONGVARCHAR` and has the same limitations as `LONGVARCHAR`.  Currently, the BI Admintool permits users to enter a maximum length of up to 2,147,483,647.  However, the actual maximum supported length is 32,678.                                                                                                                                                                                                                                                                                                                                                                                                               |


Country         | IDD | NDD
--------------- | --: | --:
United Kingdom  |  00 |   0
United States   | 011 |   1

  - IDD: International Dialing Prefix
  - NDD: National Dialing Prefix

Phone Number     | Dial
---------------- | -------------------
 +1 925 694 5326 | 1 925 694 5326
+44 117 946 2685 | 011 44 117 946 2685
+91 80 5108 5046 | 011 91 80 5108 5046

## List Examples

The default graphics system for Qt Creator seems to be `raster`.

  - **raster** - Slow repaints with NoMachine NX.  Seems to be the default for Qt Creator
  - **opengl** - Not suitable for remote sessions
  - **opengl1** - Not suitable for remote sessions
  - **x11** - Results in error message `Unable to load graphicssystem "x11"`
  - **native** - Fast repaints with NoMachine NX.  This is the recommended option.
  
So, simply pass `-graphicssystem native` when starting `qtcreator`. For example,

    qtcreator -graphicssystem native

[qt-graphics-performance]: http://labs.qt.nokia.com/2009/12/16/qt-graphics-and-performance-an-overview/


1.  Set up the build environment from the shell and lauch Qt Creator.
2.  Create a new project (File > New File or Project...)

    Another paragraph of text.
3.  Choose Import Project / Import Existing Project
4.  Enter a project name and select the `biserver/analytics/server` directory
    as the **Location**
5.  Deselect all directories and then select the following:

      - cluster
      - include
      - Log
      - mallocheap
      - Metadata
      - NQSAdmin (optional)
      - NQSCom
      - NQSNative
      - NQSODBC
      - NQSScheduler (optional)
      - objectmodel
      - perf
      - Query
      - Utility

6.  Enter the following filter in the **Hide files matching** field and click **Apply Filter**

        Makefile*; *.o; *.obj; *~; *.files; *.config; *.creator; *.user; *.includes; *#*; *.dsp; *.ncb; *.sln; *.suo; *.vcproj*; *.vcxproj*

7.  Skip the project management / version control settings and click **Finish**

Some useful shortcuts in Qt Creator:

   - `Ctrl+L` - Go to line
   - `Alt-V, Alt-I` - open in Emacs
   - `Alt-V, Alt-V` - Check out in ADE
   - `Ctrl-K` - Activate locator

See [Qt Creator Keyboard Shortcuts][qtc-shortcuts] for a complete list of
default shortcuts.

1.  Set up the build environment from the shell and lauch Qt Creator.
    1.  Set up the build environment from the shell and lauch Qt Creator.
    2.  Create a new project (File > New File or Project...)
    3.  Choose Import Project / Import Existing Project
    4.  Enter a project name and select the `biserver/analytics/server` directory
        as the **Location**
2.  Create a new project (File > New File or Project...)
    1.  Set up the build environment from the shell and lauch Qt Creator.
    2.  Create a new project (File > New File or Project...)
        1.  Set up the build environment from the shell and lauch Qt Creator.
        2.  Create a new project (File > New File or Project...)
        3.  Choose Import Project / Import Existing Project
        4.  Enter a project name and select the `biserver/analytics/server` directory
            as the **Location**
    3.  Choose Import Project / Import Existing Project
    4.  Enter a project name and select the `biserver/analytics/server` directory
        as the **Location**
3.  Choose Import Project / Import Existing Project
4.  Enter a project name and select the `biserver/analytics/server` directory
    as the **Location**

[qtc-shortcuts]: http://doc.qt.nokia.com/qtcreator-2.5/creator-keyboard-shortcuts.html

## Callouts

### Note

Note: This is a not a callout.

**Note:** This is a legacy-style **note** callout.

> [!NOTE]
> This is a GFM-style **note** callout.

### Issue

Issue: This is not an issue callout.

**Issue:** This is a legacy-style **issue** callout.

> [!ISSUE]
> This is a GFM-style **issue** callout.

### Hint

Hint: This is not a hint callout.

**Hint:** This is a legacy-style **hint** callout.

> [!HINT]
> This is a GFM-style **hint** callout.

### Tip

Tip: This is not a tip callout.

**Tip:** This is a legacy-style **tip** callout.

> [!TIP]
> This is a GFM-style **tip** callout.

### Important

Important: This is not an important callout.

**Important:** This is a legacy-style **important** callout.

> [!IMPORTANT]
> This is a GFM-style **important** callout.

### Warning

Warning: This is not a warning callout.

**Warning:** This is a legacy-style **warning** callout.

> [!WARNING]
> This is a GFM-style **warning** callout.

### Caution

Caution: This is not a caution callout.

**Caution:** This is a legacy-style **caution** callout.

> [!CAUTION]
> This is a GFM-style **caution** callout.

### Mixed Case Callout Markers

The callout marker (e.g., `[!NOTE]`) is case insensitive. So, all of the following
are valid note callouts.

> [!Note] This is a **note** callout

> [!NOTE] This is a **note** callout

> [!note] This is a **note** callout

### Invalid Callout Type

Foo: This is not a callout.

**Foo:** This is a legacy-style callout with an invalid callout type.

> [!FOO]
> This is a GFM-style callout with an invalid callout type.

### Nested Content in Callouts

> [!NOTE]
> This is a GFM-style note callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!ISSUE]
> This is a GFM-style issue callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!HINT]
> This is a GFM-style hint callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!TIP]
> This is a GFM-style tip callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!IMPORTANT]
> This is a GFM-style important callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!WARNING]
> This is a GFM-style warning callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

> [!CAUTION]
> This is a GFM-style caution callout with nested content.
> 
> Note: The following correction should be made to step 4 from
> [Adding the Parent-Child Relationship Table to the Model][1]. Instead of
> clicking on the the logical table source of the **dimension** table it should
> be the `logical table source` of the **fact** table.
> 
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted
> 
> ```html
> <html>
>   <head>
>     <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
>   </head>
> </html>
> ```
> 
> > In the Business Model and Mapping layer, double-click the logical table
> > source for the logical **fact** table that is used in your parent-child
> > `hierarchy`.  See also [Some Related Note][1].
> >
> >     Preformatted preformatted preformatted preformatted preformatted preformatted
> >     Preformatted preformatted preformatted preformatted preformatted preformatted

### Div-wrapped Callouts

<div class="note">

Note: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="issue">

Issue: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="hint">

Hint: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="tip">

Tip: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="important">

Important: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="warning">

Warning: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

<div class="caution">

Caution: The following correction should be made to step 4 from
[Adding the Parent-Child Relationship Table to the Model][1].  Instead of
clicking on the the logical table source of the **dimension** table it should
be the `logical table source` of the **fact** table.

    Preformatted preformatted preformatted preformatted preformatted preformatted
    Preformatted preformatted preformatted preformatted preformatted preformatted

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

> In the Business Model and Mapping layer, double-click the logical table
> source for the logical **fact** table that is used in your parent-child
> `hierarchy`.  See also [Some Related Note][1].
>
>     Preformatted preformatted preformatted preformatted preformatted preformatted
>     Preformatted preformatted preformatted preformatted preformatted preformatted

</div>

## Code

HTML:

```html
<html>
  <head>
    <meta http-equiv="REFRESH" content="0;url=/servertests"></HEAD>
  </head>
</html>
```

CSS:

```css
body {
    font-family: sans-serif;
    font-size: 100%;
    background: #fff;
/*     margin: 20px; */
    color: #000;
}
h1 {
    font-family: sans-serif;
    font-size: 1.6em;
    color: #656665;
    border-bottom: 4px solid #f00;
}
```

C++:

```cpp
void NQDbGateway::Prepare()
{

    NQString desc = m_identifier + L"DbGateway Prepare";
    MonitoredNQDbGateway mon(desc.c_str(),m_sql,m_connectionPoolName,m_signon.m_userName);
    su::ActivityGuard monGuard(mon);
    TraceGateway(SUText("DbGateway Prepare"), m_identifier, m_connectionPoolName);

    ResourceGuard releaseOnError(this);

    // ...

}
```

Java:

```java
import java.sql.*;
class Conn {
    public static void main (String[] args) throws Exception
    {
        Class.forName ("oracle.jdbc.OracleDriver");

        Connection conn = DriverManager.getConnection
            ("jdbc:oracle:thin:@//localhost:1521/orcl", "scott", "tiger");
                              // @//machineName:port/SID,   userid,  password
        try {
            Statement stmt = conn.createStatement();
            try {
                ResultSet rset = stmt.executeQuery("select BANNER from SYS.V_$VERSION");
                try {
                    while (rset.next())
                        System.out.println (rset.getString(1));   // Print col 1
                } 
                finally {
                    try { rset.close(); } catch (Exception ignore) {}
                }
            } 
            finally {
                try { stmt.close(); } catch (Exception ignore) {}
            }
        } 
        finally {
            try { conn.close(); } catch (Exception ignore) {}
        }
    }
}
```

SQL:

```sql
// Q1: This query returns 36 for every [Year, Month] even though some items are not present for some months.
select
  "Global"."Time"."Year",
  "Global"."Time"."Month",
  count(distinct "Global"."Product"."ItemID")
from "Global"
where Time.Year = 1998
order by 1, 2;
```

Shell script:

```sh
for x in $(ade showlabels -series BIFNDN_11.1.1_LINUX.X64) $(ade showlabels -series BISERVER_11.1.1_LINUX.X64) ; do
    echo $x
    dir=$(/usr/local/packages/intg/bin/iprint Label_Rdd_Path --label $x 2> /dev/null | head -1)
    file=$dir/*/analytics/server/include/objectmodel/SOARpGateway.h
    if [ ! -f $file ] ; then
        continue
    fi
    grep "define NQUIRE_REPOSITORY_CURRENT_VERSION" $file | sed -e 's/.*[^0-9]\([0-9][0-9]*\)/\1/'
done 
```

> Quoted code:
> 
>> <?prettify lang=sql?>
>>
>>     // Q1: This query returns 36 for every [Year, Month] even though some items are not present for some months.
>>     select
>>       "Global"."Time"."Year",
>>       "Global"."Time"."Month",
>>       count(distinct "Global"."Product"."ItemID")
>>     from "Global"
>>     where Time.Year = 1998
>>     order by 1, 2;
>>
>>     // Q2: If Units is added to the projection, then each month returns a different count.
>>     select
>>       "Global"."Time"."Year",
>>       "Global"."Time"."Month",
>>       "Global"."UnitsCube"."Units",
>>       count(distinct "Global"."Product"."ItemID")
>>     from "Global"
>>     where Time.Year = 1998
>>     order by 1, 2;

[1]: #

<?xml version='1.0' encoding='utf-8'?>

<!-- XHTML-to-XHTML converter by Fletcher Penney
	specifically designed for use with MultiMarkdown created XHTML
	
	Adds a Table of Contents to the top of the XHTML document,
	and adds linkbacks from h1 and h2's.
	
	Also, an example of the sorts of things that can be done to customize
	the XHTML output of MultiMarkdown.
	
	MultiMarkdown Version 2.0.b6
	
	$Id: xhtml-toc.xslt 499 2008-03-23 13:03:19Z fletcher $

	TODO: If a section has no children, a "<ol></ol>" is generated, which is invalid
-->

<!-- 
# Copyright (C) 2007-2008  Fletcher T. Penney <fletcher@fletcherpenney.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
#    Free Software Foundation, Inc.
#    59 Temple Place, Suite 330
#    Boston, MA 02111-1307 USA
-->


<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
  	exclude-result-prefixes="xhtml xsl"
	version="1.0">

	<xsl:variable name="newline">
<xsl:text>
</xsl:text>
	</xsl:variable>

	<xsl:output method='xml' version="1.0" encoding='utf-8' doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" indent="no"/>

	<!-- the identity template, based on http://www.xmlplease.com/xhtmlxhtml -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- adjust the body to add a ToC -->
	<!-- TODO: Will need to move this to just before first <h1> to allow for introductory comments -->
	<xsl:template match="h1[1]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
                <xsl:value-of select="$newline"/>
                <div class="toc">
                <xsl:value-of select="$newline"/>
                <table>
                <xsl:value-of select="$newline"/>
                <thead><th>Table of Contents</th></thead>
                <xsl:value-of select="$newline"/>
                <tbody>
                <xsl:value-of select="$newline"/>
                <tr><td>
                <xsl:value-of select="$newline"/>
                <ul>
                        <xsl:apply-templates select="/html/body" mode="ToC"/>
                        <xsl:value-of select="$newline"/>
                </ul>
                <xsl:value-of select="$newline"/>
                </td></tr>
                <xsl:value-of select="$newline"/>
                </tbody>
                <xsl:value-of select="$newline"/>
                </table>
                <xsl:value-of select="$newline"/>
                </div>
	</xsl:template>


	<!-- create ToC entry -->
	<xsl:template match="body" mode="ToC">
		<xsl:apply-templates select="h1" mode="ToC"/>
	</xsl:template>

	<xsl:template match="h1" mode="ToC">
		<xsl:value-of select="$newline"/>
		<xsl:variable name="link">
			<xsl:value-of select="@id"/>
		</xsl:variable>
		<xsl:variable name="myId">
			<xsl:value-of select="generate-id(.)"/>
		</xsl:variable>
		<li>
			<a id="ToC-{$link}" href="#{$link}">
				<xsl:apply-templates select="node()"/>
			</a>
			<xsl:if test="following::h2[1][preceding::h1[1]]">
				<xsl:value-of select="$newline"/>
				<ul>
					<xsl:apply-templates select="following::h2[preceding::h1[1][generate-id() = $myId]]" mode="ToC"/>
					<xsl:value-of select="$newline"/>
				</ul>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</li>
	</xsl:template>

	<xsl:template match="h2" mode="ToC">
		<xsl:value-of select="$newline"/>
		<xsl:variable name="link">
			<xsl:value-of select="@id"/>
		</xsl:variable>
		<xsl:variable name="myId">
			<xsl:value-of select="generate-id(.)"/>
		</xsl:variable>
		<li>
			<a id="ToC-{$link}" href="#{$link}">
				<xsl:apply-templates select="node()"/>
			</a>
			<xsl:if test="following::h3[1][preceding::h2[1]]">
				<xsl:value-of select="$newline"/>
				<ul>
					<xsl:apply-templates select="following::h3[preceding::h2[1][generate-id() = $myId]]" mode="ToC"/>
					<xsl:value-of select="$newline"/>
				</ul>
				<xsl:value-of select="$newline"/>
			</xsl:if>
		</li>
	</xsl:template>

	<xsl:template match="h3" mode="ToC">
		<xsl:value-of select="$newline"/>
		<xsl:variable name="link">
			<xsl:value-of select="@id"/>
		</xsl:variable>
		<li>
			<a id="ToC-{$link}" href="#{$link}">
				<xsl:apply-templates select="node()"/>
			</a>
		</li>
	</xsl:template>

</xsl:stylesheet>

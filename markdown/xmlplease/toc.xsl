<?xml version="1.0"?>
<!-- toc.xsl generate the TOC and the links from TOC items to headings -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xhtml">

	<xsl:template name="toc">
		<xsl:for-each-group
			select="//xhtml:h1|//xhtml:h2|//xhtml:h3|//xhtml:h4|//xhtml:h5|//xhtml:h6"
			group-starting-with="xhtml:h1">
			<xsl:apply-templates select="." mode="toc"/>
		</xsl:for-each-group>
	</xsl:template>

	<xsl:template match="xhtml:h1" mode="toc">
		<xsl:if test="following::xhtml:h2[1][preceding::xhtml:h1[1] = current-group()]">
			<ol>
				<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h2">
					<xsl:apply-templates select="." mode="toc"/>
				</xsl:for-each-group>
			</ol>
		</xsl:if>
	</xsl:template>

	<xsl:template match="xhtml:h2" mode="toc">
		<xsl:variable name="nh2"><xsl:number/>.</xsl:variable>
		<li class="first">
			<a href="#s{$nh2}">
				<xsl:value-of select="."/>				
			</a>
			<xsl:if test="following::xhtml:h3[1][preceding::xhtml:h2[1] = current-group()]">
				<ol>
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h3">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h3" mode="toc">
		<xsl:variable name="nh3">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any"/>
		</xsl:variable>
		<li>
			<a href="#s{$nh3}">
				<xsl:value-of select="."/>
			</a>
			<xsl:if test="following::xhtml:h4[1][preceding::xhtml:h3[1]  = current-group()]">
				<ol>
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h4">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h4" mode="toc">
		<xsl:variable name="nh4">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any"/>
		</xsl:variable>
		<li>
			<a href="#s{$nh4}">
				<xsl:value-of select="."/>
			</a>
			<xsl:if test="following::xhtml:h5[1][preceding::xhtml:h4[1]  = current-group()]">
				<ol>
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h5">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h5" mode="toc">
		<xsl:variable name="nh5">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
		<li>
			<a href="#s{$nh5}">
				<xsl:value-of select="."/>
			</a>
			
			<xsl:if test="following::xhtml:h6[1][preceding::xhtml:h5[1]  = current-group()]">
				<ol>
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h6">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h6" mode="toc">
		<li><xsl:variable name="nh6">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
			
				<a href="#s{$nh6}">
					<xsl:value-of select="."/>
				</a>
				
			<xsl:if test="following::xhtml:h7[1][preceding::xhtml:h6[1]  = current-group()]">
				<ol>
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h7">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ol>
			</xsl:if>
		</li>
	</xsl:template>

	<xsl:template match="xhtml:h2">
		<xsl:variable name="nh2"><xsl:number/>.</xsl:variable>
		<h2 id="s{$nh2}">
			<xsl:apply-templates select="@*|node()"/>
		</h2>
	</xsl:template>

	<xsl:template match="xhtml:h3">
		<xsl:variable name="nh3">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any"/>
		</xsl:variable>
		<h3 id="s{$nh3}">
			<xsl:apply-templates select="@*|node()"/>
		</h3>
	</xsl:template>
	
	<xsl:template match="xhtml:h4">
		<xsl:variable name="nh4">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any"/>
		</xsl:variable>
		<h4 id="s{$nh4}">
			<xsl:apply-templates select="@*|node()"/>
		</h4>
	</xsl:template>

	<xsl:template match="xhtml:h5">
		<xsl:variable name="nh5">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
		<h5 id="s{$nh5}">
			<xsl:apply-templates select="@*|node()"/>
		</h5>
	</xsl:template>
	
	<xsl:template match="xhtml:h6">
		<xsl:variable name="nh6">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
		<h6 id="s{$nh6}">
			<xsl:apply-templates select="@*|node()"/>
		</h6>
	</xsl:template>
	
<!-- Look in source code for all details -->
</xsl:stylesheet>

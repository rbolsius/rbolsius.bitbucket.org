<?xml version="1.0"?>
<!-- identity.xsl includes toc.xsl -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml">
<!-- Here we might need the strip-space and the preserve-space elements -->	
<xsl:include href="toc.xsl"/>
	<xsl:template match="/">
		<xsl:result-document method="xml" href="myxhtml.html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" indent="yes">
			<xsl:apply-templates select="node()"/>
		</xsl:result-document>
	</xsl:template>

	<!-- Identity template -->
	<xsl:template match="@*|node()">		
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>	
	</xsl:template>
	
	<!-- The following template finds the node in the input document after which the TOC should be inserted -->	
	<!-- In the example, we want the TOC to be placed after the first paragraph -->
	<xsl:template match="xhtml:p[1]">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>	
		<xsl:call-template name="toc"/>		
	</xsl:template>
	
	<!-- Look in source code to see all details -->
</xsl:stylesheet>

<?xml version="1.0"?>
<!-- toc-raw.xsl generates the TOC without links and numbers -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml">
<xsl:template name="toc">
<xsl:for-each-group select="//xhtml:h1|//xhtml:h2|//xhtml:h3|//xhtml:h4|//xhtml:h5|//xhtml:h6" group-starting-with="xhtml:h1">
			<xsl:apply-templates select="." mode="toc"/>
		</xsl:for-each-group>
</xsl:template>

	<xsl:template match="xhtml:h1" mode="toc">
		<xsl:if test="following::xhtml:h2[1][preceding::xhtml:h1[1] = current-group()]">
			<ul>
				<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h2">
					<xsl:apply-templates select="." mode="toc"/>
				</xsl:for-each-group>
			</ul>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="xhtml:h2" mode="toc">
		<li>
			<xsl:value-of select="."/>
			<xsl:if test="following::xhtml:h3[1][preceding::xhtml:h2[1] = current-group()]">
				<ul>
					<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h3">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h3" mode="toc">
		<li>
			<xsl:value-of select="."/>
			<xsl:if test="following::xhtml:h4[1][preceding::xhtml:h3[1]  = current-group()]">
				<ul>
					<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h4">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h4" mode="toc">
		<li>
			<xsl:value-of select="."/>
			<xsl:if test="following::xhtml:h5[1][preceding::xhtml:h4[1]  = current-group()]">
				<ul>
					<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h5">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h5" mode="toc">
		<li>
			<xsl:value-of select="."/>
			<xsl:if test="following::xhtml:h6[1][preceding::xhtml:h5[1]  = current-group()]">
				<ul>
					<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h6">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	
	<xsl:template match="xhtml:h6" mode="toc">
		<li>
			<xsl:value-of select="."/>			
		</li>
	</xsl:template>
	
	<!-- Look in source code for all details -->
</xsl:stylesheet>
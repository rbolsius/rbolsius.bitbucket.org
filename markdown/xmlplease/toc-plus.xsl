<?xml version="1.0"?>
<!-- toc-plus.xsl generates the TOC, the links and the numbers -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xs xhtml">

	<xsl:template name="toc">
		<xsl:for-each-group
			select="//xhtml:h1|//xhtml:h2|//xhtml:h3|//xhtml:h4|//xhtml:h5|//xhtml:h6"
			group-starting-with="xhtml:h1">
			<xsl:apply-templates select="." mode="toc"/>
		</xsl:for-each-group>
	</xsl:template>

	<xsl:template match="xhtml:h1" mode="toc">
		<xsl:if test="following::xhtml:h2[1][preceding::xhtml:h1[1] = current-group()]">
			<ul id="itoc" class="itoc">
				<xsl:for-each-group select="current-group() except ." group-starting-with="xhtml:h2">
					<xsl:apply-templates select="." mode="toc"/>
				</xsl:for-each-group>
			</ul>
		</xsl:if>
	</xsl:template>

	<xsl:template match="xhtml:h2" mode="toc">
		<xsl:variable name="nh2">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
		</xsl:variable>
		<xsl:variable name="pos">
			<xsl:number level="any" count="xhtml:h2|xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6"/>
		</xsl:variable>
		<li>
			<span>
				<xsl:if test="$pos mod 2 ne 0">
					<xsl:attribute name="class">altc</xsl:attribute>
				</xsl:if>
				<span>
					<xsl:if test="xs:integer(substring-before($nh2, '.')) lt 10">&#160;</xsl:if>
					<xsl:value-of select="$nh2"/>
				</span>
				<xsl:text> </xsl:text>
				<a href="#s{$nh2}">
					<xsl:value-of select="."/>
				</a>
			</span>
			<xsl:if test="following::xhtml:h3[1][preceding::xhtml:h2[1] = current-group()]">
				<ul class="itoc">
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h3">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>

		</li>
	</xsl:template>
	<xsl:template match="xhtml:h3" mode="toc">
		<xsl:variable name="nh3">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any"/>
		</xsl:variable>
		<xsl:variable name="pos">
			<xsl:number level="any" count="xhtml:h2|xhtml:h3|xhtml:h4|xhtml:h5|xhtml:h6"/>
		</xsl:variable>
		<li>
			<span>
				<xsl:if test="$pos mod 2 ne 0">
					<xsl:attribute name="class">altc</xsl:attribute>
				</xsl:if>

				<span>
					<xsl:if test="xs:integer(substring-before($nh3, '.')) lt 10">&#160;</xsl:if>
					<xsl:value-of select="$nh3"/>
				</span>
				<xsl:text> </xsl:text>
				<a href="#s{$nh3}">
					<xsl:value-of select="."/>
				</a>
			</span>
			<xsl:if test="following::xhtml:h4[1][preceding::xhtml:h3[1]  = current-group()]">
				<ul class="itoc">
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h4">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="xhtml:h4" mode="toc">
		<xsl:variable name="nh4">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any"/>
		</xsl:variable>
		<li>
			<span>
				<xsl:if test="xs:integer(substring-before($nh4, '.')) lt 10">&#160;</xsl:if>
				<xsl:value-of select="$nh4"/>
			</span>
			<xsl:text> </xsl:text>
			<a href="#s{$nh4}">
				<xsl:value-of select="."/>
			</a>
			<xsl:if test="following::xhtml:h5[1][preceding::xhtml:h4[1]  = current-group()]">
				<ul class="itoc">
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h5">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="xhtml:h5" mode="toc">
		<xsl:variable name="nh5">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
		<li>
			<span>
				<xsl:if test="xs:integer(substring-before($nh5, '.')) lt 10">&#160;</xsl:if>
				<xsl:value-of select="$nh5"/>
			</span>
			<xsl:text> </xsl:text>
			<a href="#s{$nh5}">
				<xsl:value-of select="."/>
			</a>

			<xsl:if test="following::xhtml:h6[1][preceding::xhtml:h5[1]  = current-group()]">
				<ul class="itoc">
					<xsl:for-each-group select="current-group() except ."
						group-starting-with="xhtml:h6">
						<xsl:apply-templates select="." mode="toc"/>
					</xsl:for-each-group>
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="xhtml:h6" mode="toc">
		<xsl:variable name="nh6">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any" format="1."/>
			<xsl:number count="xhtml:h6" from="xhtml:h5" level="any"/>
		</xsl:variable>
		<li>
			<span>
				<xsl:if test="xs:integer(substring-before($nh6, '.')) lt 10">&#160;</xsl:if>
				<xsl:value-of select="$nh6"/>
			</span>
			<xsl:text> </xsl:text>
			<a href="#s{$nh6}">
				<xsl:value-of select="."/>
			</a>
		</li>
	</xsl:template>

	<xsl:template match="xhtml:h2">
		<xsl:variable name="nh2">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
		</xsl:variable>
		<h2 id="s{$nh2}">
			<span>
				<xsl:value-of select="$nh2"/>
			</span>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="@*|node()"/>
		</h2>
	</xsl:template>

	<xsl:template match="xhtml:h3">
		<xsl:variable name="nh3">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any"/>
		</xsl:variable>
		<h3 id="s{$nh3}">
			<span>
				<xsl:value-of select="$nh3"/>
			</span>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="@*|node()"/>
		</h3>
	</xsl:template>

	<xsl:template match="xhtml:h4">
		<xsl:variable name="nh4">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any"/>
		</xsl:variable>
		<h4 id="s{$nh4}">
			<span>
				<xsl:value-of select="$nh4"/>
			</span>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="@*|node()"/>
		</h4>
	</xsl:template>

	<xsl:template match="xhtml:h5">
		<xsl:variable name="nh5">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any"/>
		</xsl:variable>
		<h5 id="s{$nh5}">
			<span>
				<xsl:value-of select="$nh5"/>
			</span>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="@*|node()"/>
		</h5>
	</xsl:template>

	<xsl:template match="xhtml:h6">
		<xsl:variable name="nh6">
			<xsl:number count="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h3" from="xhtml:h2" level="any" format="1."/>
			<xsl:number count="xhtml:h4" from="xhtml:h3" level="any" format="1."/>
			<xsl:number count="xhtml:h5" from="xhtml:h4" level="any" format="1."/>
			<xsl:number count="xhtml:h6" from="xhtml:h5" level="any"/>
		</xsl:variable>
		<h6 id="s{$nh6}">
			<span>
				<xsl:value-of select="$nh6"/>
			</span>
			<xsl:text> </xsl:text>
			<xsl:apply-templates select="@*|node()"/>
		</h6>
	</xsl:template>
<!-- Look in source code for all details -->
</xsl:stylesheet>
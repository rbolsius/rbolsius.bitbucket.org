Title:  Markdown  
Author: Roger Bolsius  
CSS:    markdown.css  
Parent: index.html  

# Table Formatting Test Cases

## Table Without Leading Column Delimiters

one | two
-:|
1 |
12 | abc \| def
123 | abc `|` def
1234 | ``double | ` backtick``
12345 | ```triple | `` backtick```

## Table with Leading Column Delimiters

| one | two
| -:|
| 1 |
| 12 | abc \| def
| 123 | abc `|` def
| 1234 | ``double | ` backtick``
| 12345 | ```triple | `` backtick```

## Indented Tables

1.  Indented table without leading column delimiters:

    one | two
    -:|
    1 |
    12 | abc \| def
    123 | abc `|` def
    1234 | ``double | ` backtick``
    12345 | ```triple | `` backtick```

2.  Indented table with leading column delimiters:

    | one | two
    | -:|
    | 1 |
    | 12 | abc \| def
    | 123 | abc `|` def
    | 1234 | ``double | ` backtick``
    | 12345 | ```triple | `` backtick```

## Table with Spanned Columns and Section Breaks

| | Grouping ||
| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content | *Long Cell* ||
| Content | **Cell** | Cell |

| New section | More | Data |
| And more | And more ||

What it should look like when formatted:

|              |          Grouping           ||
| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content      |         *Long Cell*         ||
| Content      |   **Cell**    |         Cell |

| New section  |     More      |         Data |
| And more     |          And more           ||

## Negative Test Cases

### No Header Table

This is a negative test case since a table without a header is not universally
accepted as a valid Markdown table and hence should not be formatted.

| 1 |
| 12 | abc
| 123 | def

### Unterminated Preformatted String

| one | two            |
| --: | -------------- |
|   1 | `preformatted  |
|  12 | abc            |
| 123 | abc `\` def    |
|  12 \
| abc |

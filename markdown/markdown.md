Title:  Markdown  
Author: Roger Bolsius  
CSS:    markdown.css  
Parent: index.html  

# Markdown

## MultiMarkdown

[MultiMarkdown][], or MMD, is a tool to help turn minimally marked-up plain
text into well formatted documents, including HTML, PDF (by way of LaTeX),
OPML, or OpenDocument (specifically, Flat OpenDocument or '.fodt', which can
in turn be converted into RTF, Microsoft Word, or virtually any other
word-processing format).

MMD is a superset of the Markdown syntax, originally created by John
Gruber. It adds multiple syntax features (tables, footnotes, and citations, to
name a few), in addition to the various output formats listed above (Markdown
only creates HTML). Additionally, it builds in "smart" typography for various
languages (proper left- and right-sided quotes, for example).

MultiMarkdown was originally a fork of the Markdown Perl code, but as of
version 3.0 has been rewritten as [a fork of peg-markdown][peg-multimarkdown]
by John MacFarlane, written in C. It can be compiled for any major operating
system, and as a native binary runs much faster than the Perl version it
replaces. Many thanks to John for creating a great program, and sharing the
source with the github community. MultiMarkdown 3.0 would not be possible
without his efforts!

For another description of what MultiMarkdown is, you can also check out a PDF
slide show that describes and demonstrates how MultiMarkdown can be used.

### Converting MultiMarkdown Files to HTML

The `mmd` shell script below invokes the multimarkdown executable to convert
the markdown text read from stdin to HTML that is written to stdout.

```shell
#!/bin/bash

dir="$(dirname "$0")"
$dir/multimarkdown --nosmart -b | dos2unix | xsltproc $dir/xhtml-table-toc.xslt -
```

This removes the extra `^M` characters that are inserted by `multimarkdown` on
Windows.  It also disables the conversion of characters like quotation marks
to "smart" quotation marks.

Finally, the script inserts a table of contents into the generated HTML
document.

The `xhtml-table-toc.xslt` is based on the [xhtml-toc.xslt][mmd-xhtml-toc]
stylesheet from the MultiMarkdown github repository, which was derived from
the [tocxhtml][] "How to" on http://xmlplease.com/.

### Adding a Table of Contents to the Converted HTML Files

Using xsltproc from [The XSLT C library for GNOME][gnome-xslt] apply the
`xhtml-table-toc.xsl` stylesheet to the original HTML.

    xsltproc xhtml-table-toc.xslt input.html > output.html

xsltproc is available on Windows either via Cygwin or as a separate
MinGW-based [Windows port][libxml-win].

## Markdown Mode for Emacs

There is a Markdown editing mode for Emacs called [markdown-mode][].  This
package can be customized to invoke the [peg-multimarkdown][] command line
utility to convert the markdown to HTML.  Just load the markdown-mode package
and set the `markdown-command` to point to the `multimarkdown` executable or a
wrapper script for the utility.

    (setq markdown-command "~/bin/mmd)

[MultiMarkdown]: http://fletcherpenney.net/multimarkdown/
[peg-multimarkdown]: https://github.com/fletcher/peg-multimarkdown
[gnome-xslt]: http://xmlsoft.org/XSLT/index.html
[libxml-win]: http://www.zlatkovic.com/libxml.en.html
[markdown-mode]: http://jblevins.org/projects/markdown-mode/
[mmd-xhtml-toc]: https://github.com/fletcher/MultiMarkdown/blob/master/XSLT/xhtml-toc.xslt
[tocxhtml]: http://xmlplease.com/tocxhtml

'use strict';

(function(window, document) {
  let useLocalStorage = false;

  // Table of contents
  let tocEntries = [];
  let tocPosition;
  let tocEnableScroll = false;
  let tocTitleHeight = 50;
  let tocLinksTop = 44;
  let tocElement;
  let tocHover = false;

  try {
    if (typeof Storage !== "undefined" && localStorage) {
      useLocalStorage = true;
    }
  } catch (e) {}

  function setTheme(theme) {
    document.documentElement.className = theme;
    if (useLocalStorage) {
      localStorage.setItem("theme", theme);
    } else {
      document.cookie = "theme=" + theme + ";path=/";
    }
    let label = document.querySelector('label[for="themeToggle"]');
    if (!label) return;
    if (theme === "dark") {
      label.title = "Always use dark theme";
    } else if (theme === "light") {
      label.title = "Always use light theme";
    } else {
      label.title = "Use system default color scheme";
    }
  }

  function toggleThemeInputElement() {
    return document.querySelector("input#themeToggle");
  }

  function toggleTheme() {
    let theme = document.documentElement.className;
    if (theme === "dark") {
      theme = "default";
    } else if (theme === "light") {
      theme = "dark";
    } else {
      theme = "light";
    }
    setTheme(theme);
  }

  function getTheme() {
    let theme;
    if (useLocalStorage) {
      theme = localStorage.getItem("theme");
    } else {
      theme = document.cookie.replace(/(?:(?:^|.*;\s*)theme\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    }
    if (theme !== "light" && theme !== "dark") {
      theme = "default";
    }
    return theme;
  }

  function initializeTheme() {
    let theme = getTheme();
    setTheme(theme);
  }

  function initializeThemeEventHandlers() {
    document.querySelector("input#themeToggle").onclick = toggleTheme;
    initializeTheme();
  }

  function isTOCLinkVisible(link) {
    let rect     = link.getBoundingClientRect(),
        vWidth   = window.innerWidth || doc.documentElement.clientWidth,
        vHeight  = window.innerHeight || doc.documentElement.clientHeight;

    return rect.top >= tocLinksTop && rect.bottom < vHeight;
  }

  function tocSetActive(active) {
    let activeClass = 'active';
    if (active) {
      this.link.classList.add(activeClass);
      if (!tocHover && tocPosition === 'fixed' && !isTOCLinkVisible(this.link))
      {
        this.link.scrollIntoView();
      }
    } else {
      this.link.classList.remove(activeClass);
    }
  }

  function resizeTOC(e) {
    if (!tocElement) return;
    let style = getComputedStyle(tocElement);
    tocPosition = style.position;
  }

  function tocEnableScrollEvents(enable) {
    if (enable != tocEnableScroll) {
      if (enable) {
        window.addEventListener('scroll', updateTOCOnScroll, false);
      } else {
        window.removeEventListener('scroll', updateTOCOnScroll, false);
      }
      tocEnableScroll = enable;
    }
  }

  function initializeTOC() {
    tocElement = document.querySelector('.toc');
    if (!tocElement) return;

    tocElement.onmouseover = function () {
      tocHover = true;
    };

    tocElement.onmouseout = function () {
      tocHover = false;
    };

    let tocLinks = document.querySelectorAll('.toc a');

    let len = tocLinks.length;
    for (let i = 0; i < len; i++) {
      let link = tocLinks[i];
      let id, h, entry;
      id = link.id.replace("ToC-", "");
      if (id === "") {
        continue;
      }
      h = document.getElementById(id);
      if (h === null) {
        console.log("Could not find heading for #" + id)
        continue;
      }
      entry = { "link": link, "heading": h, "offsetTop": h.offsetTop, "setActive": tocSetActive };
      if (i == 0) {
        link.href = location.toString().replace(/#.*/, "");
      }
      tocEntries.push(entry);
    }
    resizeTOC();
    tocEnableScrollEvents(true);
    updateTOCOnScroll();
  }

  function updateTOCOnHashChange(e) {
    if (tocEntries.length === 0) return;
    tocEnableScrollEvents(false);

    let fragment = window.location.hash.substr(1);
    let foundLink = false;

    let len = tocEntries.length;
    for (let i = 0; i < len; i++) {
      let entry = tocEntries[i];
      if (!foundLink && entry.heading.id === fragment) {
        entry.setActive(true);
        foundLink = true;
        if (i == 0) {
          window.scrollTo(0, 0);
        }
      } else {
        entry.setActive(false);
      }
    }
    if (!foundLink) {
      tocEntries[0].setActive(true);
    }
    tocEnableScrollEvents(true);
  }

  function updateTOCOnScroll(e) {
    if (tocEntries.length === 0) return;
    let activeEntry = tocEntries[0];
    let scrollY = window.scrollY || window.pageYOffset;

    // console.log("scrollY: " + (scrollY));
    // console.log(activeEntry);

    if (scrollY > 0) {
      let len = tocEntries.length;
      for (let i = 0; i < len; i++) {
        let entry = tocEntries[i];
        entry.setActive(false);
        // console.log("entry.offsetTop - tocTitleHeight: " + (entry.offsetTop - tocTitleHeight));
        // console.log("scrollY >= entry.offsetTop - tocTitleHeight: " + (scrollY >= entry.offsetTop - tocTitleHeight));
        if (scrollY >= entry.offsetTop - tocTitleHeight) {
          activeEntry = entry;
        }
      }
      // console.log("activeEntry: ");
      // console.log(activeEntry);
    }
    
    activeEntry.setActive(true);
  }

  function onload() {
    initializeTOC();
    initializeThemeEventHandlers();
  }

  initializeTheme();

  window.onload = onload;
  window.onresize = resizeTOC;
  window.onhashchange = updateTOCOnHashChange;

})(window, document);

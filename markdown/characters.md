Title:  Characters  
Author: Roger Bolsius  
CSS:    markdown.css  
Parent: index.html  

# Characters

## Unusual Characters in Headings

### Heading with : Colon

### Heading with . Period

### 1 Heading Starting with Number

### 1.2.3.4

### 12345

###

### Normal
